GOOS ?= $(shell go env GOOS)
GOARCH ?= $(shell go env GOARCH)
GOBUILD=CGO_ENABLED=0 go build -trimpath

export DOCKER_BUILDKIT=1

DOCKER_REPO ?= registry.gitlab.com/gitlab-org/opstrace/clickhouse-exporter
DOCKER_TAG ?= latest

.PHONY: all
all: build test

.PHONY: build
build:
	go install -v
	${GOBUILD}

.PHONY: test
test:
	go test -v -race

.PHONY: docker
docker:
	docker build -t ${DOCKER_REPO}:${DOCKER_TAG} -f Dockerfile .

.PHONY: docker-push
docker-push: docker
	docker push ${DOCKER_REPO}:${DOCKER_TAG}
