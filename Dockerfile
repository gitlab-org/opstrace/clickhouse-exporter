FROM golang:1.19.3 AS BUILD

WORKDIR /app
COPY . .
RUN make

FROM alpine:latest as CERTS

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

FROM scratch

COPY --from=CERTS /etc/ssl/certs /etc/ssl/certs
COPY --from=BUILD /app/clickhouse_exporter /usr/local/bin/clickhouse_exporter

ENTRYPOINT ["/usr/local/bin/clickhouse_exporter"]
